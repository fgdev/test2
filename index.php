<?php
	/**
	 * Created by PhpStorm.
	 * @author: Stas Panyukov <stas.panyukov@gmail.com>
	 * @date: 10/21/2017
	 *
	 * To change this template use File | Settings | File Templates.
	 */

	?>
<!doctype html>
<html>
<head>
	<meta charset="utf-8"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>

	<title>Test file upload</title>
	<link rel="stylesheet" href="main.css" />
	<link href="//fonts.googleapis.com/css?family=Roboto:300" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js"></script>
	<script>
		window.FileAPI = {
			  debug: false, // debug mode
			  staticPath: '/jquery.fileapi/FileAPI/' // path to *.swf
			  //chunkSize: 0.5 *10000,
			  //chunkUploadRetry: 3
		};
	</script>
	<script src="/jquery.fileapi/FileAPI/FileAPI.min.js"></script>
	<script src="/jquery.fileapi/FileAPI/FileAPI.exif.js"></script>
	<script src="/jquery.fileapi/jquery.fileapi.min.js"></script>
	<script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

</head>

<body>

	<div class="row ">

		<div class="col-md-12">

			<div class="" style="margin: 0 auto; width: 400px;">
				<div class="">
					<h2><span>File uploader</span></h2>
				</div>
				<div class="" >
					<div id="uploader" class="btn btn-success js-fileapi-wrapper">
						<div class="js-browse">
							<span class="btn-txt">Browse</span>
							<input type="file" name="file" />
						</div>
						<div class="js-upload" style="display: none">
							<div class="progress progress-success">
								<div class="js-progress bar"></div>
							</div>
							<span class="btn-txt">Uploading (<span class="js-size"></span>)</span>
						</div>
					</div>
				</div>



				<script>

						$('#uploader').fileapi({
							autoUpload: true,
							multiple: false,
							chunkSize: 1 * 1024 * 1024, //0.5 * FileAPI.MB,
							//chunkUploadRetry: 3,
							elements: {
								size: '.js-size',
								active: { show: '.js-upload', hide: '.js-browse' },
								progress: '.js-progress'
							},
							url: '/uploadChunked.php',
							onFileComplete: function (evt, uiEvt) {
								if (uiEvt.result.status == false && !uiEvt.result.error) {
									alert(uiEvt.result.message);
									console.log(uiEvt);
								}
								if (uiEvt.result.error) {
									alert(uiEvt.result.message);
									console.log(uiEvt);
								} else {
									var uid = FileAPI.uid(uiEvt.file);
									//jQuery(this).find('#w0').val(uiEvt.result.identity);
									//{"status":"success","identity":"59eb0d477f5b0.jpg","message":"[]"}
									//{"status":"error","message":"server error"}
									alert(uiEvt.result.message);
									console.log(uiEvt);

								}
							},
							onFileRemove: function (evt, file) {
							},
							onFileRemoveCompleted: function (evt, file) {

							}
						});


				</script>
			</div>

		</div>

	</div>

</body>
</html>

