<?
	$maxFileSize = 50 * 1024 * 1024;

	function getExt($filename)
	{
		return end(explode(".", $filename));
	}



	$result = ['status' => false, 'message' => 'Файл не был загружен'];
	if ($_FILES['file'])
	{
		if($_FILES['file']['error'])
		{
			$message = "Ошибка загрузки";
		}
		if(($_FILES['file'] == "none") OR (empty($_FILES['file']['name'])))
		{
			$message = "Вы не выбрали файл";
		}
		elseif($_FILES['file']["size"] == 0 OR $_FILES['file']["size"] > $maxFileSize)
		{
			$message = "Размер файла превышает 50мб";
		}
		elseif(!in_array($_FILES['file']["type"], ['application/octet-stream']))
		{
			$message = "Допускается загрузка только файлов BIN.";
		}
		elseif(!is_uploaded_file($_FILES['file']["tmp_name"]))
		{
			$message = "Что-то пошло не так. Попытайтесь загрузить файл ещё раз.";
		}
		else
		{
			$name = rand(1, 1000).'-'.md5($_FILES['file']['name']).'.'.getExt($_FILES['file']['name']);
			#echo $_SERVER['DOCUMENT_ROOT']; return;
			if(move_uploaded_file($_FILES['file']['tmp_name'],  $_SERVER['DOCUMENT_ROOT']."/upload/".$name))
			{
				$full_path = 'http://'.$_SERVER['HTTP_HOST'].'/upload/'.$name;
				$message = "Файл ".$_FILES['file']['name']." загружен";
				$result['status'] = true;
			}
			else
				$message = "Что-то пошло не так. Попытайтесь загрузить файл ещё раз!";


		}

		$result['message'] = $message;
		echo json_encode($result);
		return;
	}


	echo json_encode($result);
	return;
?>