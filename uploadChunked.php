<?


	$path = $_SERVER['DOCUMENT_ROOT'].'/upload/';

	$content_disposition_header = $_SERVER['HTTP_CONTENT_DISPOSITION'];
 	preg_match('#filename=([a-z0-9\-\_\.]+)$#ui', $content_disposition_header, $matches);
	$file_name = $matches? $matches[1] : null;

	$chunkSize = 1*1024*1024;
	$content_range_header = $_SERVER['HTTP_CONTENT_RANGE'];
	$content_range = $content_range_header?preg_split('/[^0-9]+/', $content_range_header) : null;
	$size = $content_range? $content_range[3] : 0;
	$chunks = ceil($size/$chunkSize);

	if ($chunks > 0)
	{
		$chunk =  ceil($content_range[1]/$chunkSize);

		$tmp_dir = sys_get_temp_dir();

		$tmp_filename = $tmp_dir.'/'.$file_name;

		file_put_contents($tmp_filename, fopen('php://input', 'r'), $chunk == 0? 0 : FILE_APPEND);

		if ($chunk == $chunks-1)
		{
			$success = rename($tmp_filename, $path.$file_name);
			echo json_encode(['status' => $success? true : false, 'message' => $success? 'Файл загружен' : 'Файл не загружен']);
		}
	}